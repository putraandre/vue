import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import register from '@/components/register'
import login from '@/components/login'
import biodata from '@/components/biodata'
import simulasiUnbk from '@/components/simulasiUnbk'
import simulasiDetail from '@/components/simulasiDetail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/register',
      name: 'register',
      component: register
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/biodata',
      name: 'biodata',
      component: biodata
    },
    {
      path: '/simulasiUnbk',
      name: 'simulasiUnbk',
      component: simulasiUnbk
    },
    {
      path: '/simulasiDetail',
      name: 'simulasiDetail',
      component: simulasiDetail
    },
    
  ]
})
